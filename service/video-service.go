package service

import (
	"github.com/meharifitih/go-gin-heroku/repository"

	"github.com/meharifitih/go-gin-heroku/entity"
)

type VideoService interface {
	Save(entity.Video) entity.Video
	Update(video entity.Video)
	Delete(video entity.Video)
	FindAll() []entity.Video
}

type videoService struct {
	videosRepository repository.VideoRepsitory
}

func New(repo repository.VideoRepsitory) VideoService {
	return &videoService{
		videosRepository: repo,
	}
}

func (service *videoService) Save(video entity.Video) entity.Video {
	service.videosRepository.Save(video)
	return video
}

func (service *videoService) FindAll() []entity.Video {
	return service.videosRepository.FindAll()
}

func (service *videoService) Update(video entity.Video) {
	service.videosRepository.Update(video)
}
func (service *videoService) Delete(video entity.Video) {
	service.videosRepository.Delete(video)
}
